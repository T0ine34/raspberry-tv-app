import subprocess as sp
import sys


"""
all the functions in this file are used to check the status of the system, and are non-platform specific
"""

def is_wifi_connected():
    """
    returns True if the wifi is connected, False otherwise (work on both linux and windows)
    """ 
    if sys.platform == "linux":
        return "wlan0" in str(sp.check_output(["iwgetid"])).lower()
    elif sys.platform == "win32":
        return "wlan" in str(sp.check_output(["netsh", "interface", "show", "interface"])).lower()
    else:
        raise NotImplementedError("platform not supported")


def is_eth_connected():
    """
    returns True if the ethernet is connected, False otherwise (work on both linux and windows)
    """ 
    if sys.platform == "linux":
        return "eth0" in str(sp.check_output(["iwgetid"])).lower()
    elif sys.platform == "win32":
        return "ethernet" in str(sp.check_output(["netsh", "interface", "show", "interface"])).lower()
    else:
        raise NotImplementedError("platform not supported")

def is_connected():
    """
    returns True if the system is connected to the internet, False otherwise
    """
    return is_wifi_connected() or is_eth_connected()


def reboot():
    """
    reboot the system immediately
    """
    if sys.platform == "linux":
        sp.call(["reboot"])
    elif sys.platform == "win32":
        sp.call(["shutdown", "/r", "/t", "0"])
    else:
        raise NotImplementedError("platform not supported")
    
def shutdown():
    """
    shutdown the system immediately
    """
    if sys.platform == "linux":
        sp.call(["shutdown", "now"])
    elif sys.platform == "win32":
        sp.call(["shutdown", "/s", "/t", "0"])
    else:
        raise NotImplementedError("platform not supported")


if __name__ == "__main__":
    print(is_connected())