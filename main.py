import sys

if not "__launcher__" in sys.argv:
    print("Please start the program using launcher.py")
    exit(1)


#------------------------------------------------------ IMPORTS ------------------------------------------------------#


try:
    import customtkinter as ctk #need to be installed
    import tkinter as tk

    from PIL import Image, ImageTk #need to be installed

    from typing import Callable

    from datetime import datetime
    from time import sleep
    from threading import Thread

    from osinfo import *
    from translator import Translator as tr
    
    from settings import Settings
    
    from webexplorer import open_website
    
    
except ImportError as e:
    exit(4) #restart the program if started in the launcher, and install the required modules
    
#-------------------------------------------------- INITIALISATION ---------------------------------------------------#
class InitError(Exception): pass

try:
    SETTINGS = Settings("settings.json")
    
    LANGUAGE = SETTINGS.get("language", default="en", write_default=True)
    
    TR = tr(LANGUAGE)
    
except Exception as e:
    raise InitError("Could not initialize the program") from e

def set_cursor(widget : tk.Widget, cursor : str):
    if sys.platform == "win32":
        widget.configure(cursor=f"@icons/mouse/{cursor}.cur")
    elif sys.platform == "linux":
        #TODO: add support for linux
        pass
    else:
        pass


#-------------------------------------------------- FUNCTIONS & CLASS ------------------------------------------------#

class InfoBar(ctk.CTkFrame):
    """
    display hour, date and wifi status as an image
    """
    def __init__(self, master):
        super().__init__(master)
        
        self.hour = ctk.CTkLabel(self, text=datetime.now().strftime("%H:%M:%S"), font=("Arial", 15))
        self.hour.pack(side=tk.RIGHT, padx=10)
        
        self.date = ctk.CTkLabel(self, text=datetime.now().strftime("%d/%m/%Y"), font=("Arial", 15))
        self.date.pack(side=tk.RIGHT, padx=10)
        
        self.wifi_img = ctk.CTkImage(Image.open("icons/no-wifi.png"), size=(25,25))
        self.wifi = ctk.CTkLabel(self, image=self.wifi_img, text="", width=40, height=40)
        self.wifi.pack(side=tk.LEFT, padx=10)
        
        self.time_thread = Thread(target=self.time_update)
        self.wifi_thread = Thread(target=self.wifi_update)
        
        self.time_thread.start()
        self.wifi_thread.start()
        
    def time_update(self):
        self.hour.configure(text=datetime.now().strftime("%H:%M:%S"))
        self.date.configure(text=datetime.now().strftime("%d/%m/%Y"))        
        self.after(100, self.time_update) #update every 100ms
        
    def wifi_update(self):
        if is_connected():
            self.wifi_img = ctk.CTkImage(Image.open("icons/wifi.png"), size=(25,25))
        else:
            self.wifi_img = ctk.CTkImage(Image.open("icons/no-wifi.png"), size=(25,25))
        self.wifi.configure(image=self.wifi_img)
        self.after(10000, self.wifi_update) #update every 10s
        
        
class Button(ctk.CTkFrame):
    def __init__(self, master, icon : str, text : str, command : Callable):
        super().__init__(master)
        
        self.icon = icon
        self.text = text
        self.command = command

        set_cursor(self, "clickable")
        
        self.icon = ctk.CTkImage(Image.open(self.icon), size=(100,100))
        self.icon_label = ctk.CTkLabel(self, image=self.icon, text="")
        self.icon_label.pack(padx=10, pady=10)
        
        self.label = ctk.CTkLabel(self, text=self.text, font=("Arial", 15))
        self.label.pack(padx=10, pady=10)
        
        self.bind("<Button-1>", self.command)
        self.icon_label.bind("<Button-1>", self.command)
        self.label.bind("<Button-1>", self.command)
        
class MainFrame(ctk.CTkFrame):
    """
    This will contain a list of Button objects, displayed in a grid
    """
    def __init__(self, master):
        super().__init__(master)
        
        self.buttons = []
        self.nb_columns = 5
        self.nb_rows = 5
        
        
        for i in range(self.nb_columns+1):
            self.grid_columnconfigure(i, weight=1)
        for i in range(self.nb_rows+1):
            self.grid_rowconfigure(i, weight=1)
        
        
    def add_button(self, button : Button):
        self.buttons.append(button)
        self.update()
            
    def remove_button(self, button : any):
        self.buttons.remove(button)
        self.update()
        
    def update(self):
        for child in self.winfo_children():
            child.grid_forget()
        
        for i, button in enumerate(self.buttons):
            button.grid(row=i//self.nb_columns, column=i%self.nb_columns)
        
        
class Confirm(ctk.CTkFrame):
    def __init__(self, master, text : str, command : Callable):
        super().__init__(master)
        
        self.text = text
        self.command = command
        
        self.frame = ctk.CTkFrame(self)
        self.frame.pack(expand=True, fill=tk.BOTH)
        
        set_cursor(self, "pointer")
        
        self.label = ctk.CTkLabel(self.frame, text=self.text, font=("Arial", 15))
        self.label.pack(padx=10, pady=10)
        
        self.yes_button = ctk.CTkButton(self.frame, text=TR("Yes"), command=self.yes)
        set_cursor(self.yes_button, "clickable")
        self.yes_button.pack(side=tk.LEFT, padx=10, pady=10)
        
        self.no_button = ctk.CTkButton(self.frame, text=TR("No"), command=self.no)
        set_cursor(self.no_button, "clickable")
        self.no_button.pack(side=tk.RIGHT, padx=10, pady=10)
        
    def yes(self):
        self.command()
        try:
            self.destroy()
        except:
            pass #if the window is already destroyed
        
    def no(self):
        self.destroy()

        
        
class App(ctk.CTk):
    def __init__(self):
        super().__init__()
        
        #set it in fullscreen
        self.attributes("-fullscreen", True)
        
        set_cursor(self, "pointer")
        
        self.info_bar = InfoBar(self)
        self.info_bar.place(anchor=tk.NW, x=0, y=0, relwidth=1)
        
        self.main_frame = MainFrame(self)
        self.main_frame.place(anchor=tk.NW, x=0, y=50, relwidth=1, relheight=1)
        
        self.main_frame.add_button(Button(self.main_frame, "icons/reduce.png", TR("Exit"), self.quit))
        self.main_frame.add_button(Button(self.main_frame, "icons/reboot.png", TR("Reboot"), self.reboot))
        self.main_frame.add_button(Button(self.main_frame, "icons/shutdown.png", TR("Shutdown"), self.shutdown))
        
        self.main_frame.add_button(Button(self.main_frame, "icons/apps/web/netflix.png", "Netflix", lambda event=None: open_website("netflix")))
        self.main_frame.add_button(Button(self.main_frame, "icons/apps/web/youtube.png", "Youtube", lambda event=None: open_website("youtube")))
        self.main_frame.add_button(Button(self.main_frame, "icons/apps/web/PrimeVideo.png", "Prime Video", lambda event=None: open_website("primevideo")))
        
    def quit(self, event=None):
        #pack it at the center of the screen, above all other widgets
        Confirm(self, TR("Are you sure you want to quit ?"), self.destroy).place(x=self.winfo_screenwidth()//2-150, y=self.winfo_screenheight()//2-50)
        
    def reboot(self, event=None):
        Confirm(self, TR("Are you sure you want to reboot ?"), reboot).place(x=self.winfo_screenwidth()//2-150, y=self.winfo_screenheight()//2-50)
        
    def shutdown(self, event=None):
        Confirm(self, TR("Are you sure you want to shutdown ?"), shutdown).place(x=self.winfo_screenwidth()//2-150, y=self.winfo_screenheight()//2-50)



if __name__ == "__main__":
    app = App()
    app.mainloop()