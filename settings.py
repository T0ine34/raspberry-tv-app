import json
import os.path

class Settings:
    _instances = {}
    
    def __new__(cls, path : str = "settings.json"):
        if path not in cls._instances:
            cls._instances[path] = super().__new__(cls)
        return cls._instances[path]
    
    def __init__(self, path : str = "settings.json"):
        
        self._path = path
        self._settings = {}
        
        self._autosave = True
        
        self.load()
        
    def _load_autosave(self):
        self._autosave = self.get("autosave", default=True)
        
        
    def load(self):
        try:
            with open(self._path, "r") as f:
                self._settings = json.load(f)
        except FileNotFoundError:
            self.save()
            self.load()
            
    def save(self):
        try:
            with open(self._path, "w") as f:
                json.dump(self._settings, f, indent=4)
            print(f"file '{self._path}' saved")
        except FileNotFoundError:
            os.makedirs(os.path.dirname(self._path), exist_ok=True)
            self.save()
    
    def __getitem__(self, *keys): #usage : _settings["key1", "key2", "key3"]
        result = self._settings
        for key in keys:
            result = result[key]
        return result
    
    def get(self, *keys, default=None, write_default=False):
        try:
            return self.__getitem__(*keys)
        except KeyError:
            if default is None:
                raise KeyError(f"Key '{keys[-1]}' not found")
            else:
                if write_default:
                    self.__setitem__(*keys, value=default)
                return default

    def __setitem__(self, key, value): #usage : _settings["key1", "key2", "key3"] = value
        self._settings[key] = value
        if self._autosave:
            self.save()
            
    def set(self, *keys, value):
        result = self._settings
        for key in keys[:-1]:
            if key not in result:
                result[key] = {}
            result = result[key]
        result[keys[-1]] = value
        if self._autosave:
            self.save()
            
    def __repr__(self):
        return f"Settings({self._path})"
    
    def __str__(self):
        return json.dumps(self._settings, indent=4)
    
    
    def delete(self):
        os.remove(self._path)
        self._settings = {}
        
    
    @property
    def settings(self):
        return self._settings
    
    @settings.setter
    def settings(self, value):
        raise AttributeError("You can't set the settings this way, use the __setitem__ method instead")
    
    @settings.deleter
    def settings(self):
        raise AttributeError("You can't delete the settings this way, delete the instance instead")
    
    @property
    def autosave(self):
        return self._autosave
    
    @autosave.setter
    def autosave(self, value : bool|None):
        if value is None:
            self._autosave = not self._autosave
        elif type(value) == bool:
            self._autosave = value
        else:
            raise TypeError("autosave must be a boolean or None")
        
    @autosave.deleter
    def autosave(self):
        raise AttributeError("You can't delete the autosave property")
    
    @property
    def path(self):
        return self._path
    
    @path.setter
    def path(self, value : str):
        self._path = value
        self.load()
        
    @path.deleter
    def path(self):
        raise AttributeError("You can't delete the path property")
    
    def __del__(self):
        self.__class__._instances.pop(self._path)
    
    def __enter__(self): #usage : with _settings as settings:
        return self
    
    def __exit__(self, exc_type, exc_value, traceback): 
        self.save()
    
