import json
import os.path

from deep_translator import GoogleTranslator as Translator

from progress_bar import ProgressBar

from typing import Callable


def is_system_string(string : str) -> bool:
    """
    check if this string is destined to be translated or not. (example : "__main__" doesn't need to be translated)
    """
    if string.startswith("__") and string.endswith("__"):
        return True
    return False

def is_empty_string(string : str) -> bool:
    """
    check if this string is empty or not.
    """
    return string == ""

def is_comment(string : str) -> bool:
    """
    check if this string is a comment or not.
    """
    return string.startswith("#")

def needs_translation(string : str) -> bool:
    """
    check if this string needs to be translated or not.
    """
    if is_system_string(string) or is_empty_string(string):
        return False
    return True


def parse_python_file(path : str) -> list[dict]:
    """
    Parse a python file and store each string detected in a list, with its line number.
    """
    if not os.path.isfile(path):
        raise FileNotFoundError(f"File '{path}' not found")
    strings = []
    absolute_path = os.path.abspath(path)
    with open(path, "r") as f:
        for i, line in enumerate(f.readlines(), 1):
            line = line.strip()
            if is_comment(line):
                continue
            if '"' in line:
                detected_strings = line.split('"')[1::2]                                  
                for string in detected_strings:
                    if needs_translation(string):
                        strings.append({"line": i, "file": absolute_path, "string": string})
            if "'" in line:
                detected_strings = line.split("'", 1)[1::2]
                for string in detected_strings:
                    if needs_translation(string):
                        strings.append({"line": i, "file": absolute_path, "string": string})
    return strings


def parse_json_file(path : str) -> list[dict]:
    """
    Parse a json file and store each string detected in a list, with its line number.
    (only store values, not keys)
    """
    if not os.path.isfile(path):
        raise FileNotFoundError(f"File '{path}' not found")
    strings = []
    absolute_path = os.path.abspath(path)
    with open(path, "r") as f:
        data = json.load(f)
        for key, value in data.items():
            if needs_translation(value):
                strings.append({"line": 0, "file": absolute_path, "string": value})
    return strings


def get_string_getter(path : str) -> Callable[[str], str]:
    #return the language of a file (python, json)
    LANGUAGES = {
        "py" : parse_python_file,
        "json" : parse_json_file
    }
    extension = path.split(".")[-1]
    if extension not in LANGUAGES:
        raise ValueError(f"Unsupported file extension : '{extension}'")
    return LANGUAGES[extension]


def translate_strings(strings : list[dict], tr : Translator) -> list[dict]:
    """
    Translate a list of strings using a translator.
    """
    translated_strings = []
    pb = ProgressBar(len(strings), "file : "+strings[0]["file"])
    for string in strings:
        pb += 1
        dict_ = string.copy()
        dict_["traduction"] = tr.translate(string["string"])
        translated_strings.append(dict_)
    return translated_strings


def translate_file(path : str, tr : Translator) -> list[dict]:
    """
    Translate all strings in a file using a translator.
    """
    strings = get_string_getter(path)(path)
    translated_strings = translate_strings(strings, tr)
    return translated_strings


def translate_files(paths : list[dict], tr : Translator) -> list[dict]:
    """
    Translate all strings in a list of files using a translator.
    """
    translated_strings = []
    
    pb = ProgressBar(len(paths), "language :"+tr.target)
    for path in paths:
        pb += 1
        translated_strings.extend(translate_file(path, tr))
    del pb
    return translated_strings


def create_translate_file(paths : list[dict], language : str, output_path : str = "translations/") -> None:
    """
    Create a translation file for a given language for a list of files.
    """
    if not output_path.endswith("/"):
        output_path += "/"
        
    os.makedirs(output_path, exist_ok=True)
    
    tr = Translator(source="auto", target=language)
    
    traductions = translate_files(paths, tr)
    
    with open(output_path+language+".json", "w") as f:
        json.dump(traductions, f, indent=4)
        

if __name__ == "__main__":
    import sys
    
    if "--no-console" in sys.argv:
        sys.stdout = open(os.devnull, "w")
        sys.stderr = open(os.devnull, "w")
        sys.argv.remove("--no-console")
    
    if len(sys.argv) < 5:
        print("usage: python translator_writer.py -l <language1> [language2] ... -f <file1> [file2] ...")
        sys.exit(1)
        
    languages = []
    files = []
    
    while len(sys.argv) > 1:
        if sys.argv[1] == "-l":
            while len(sys.argv) > 2 and sys.argv[2] != "-f":
                languages.append(sys.argv.pop(2))
            sys.argv.pop(1)
        elif sys.argv[1] == "-f":
            while len(sys.argv) > 2 and sys.argv[2] != "-l":
                files.append(sys.argv.pop(2))
            sys.argv.pop(1)
        else:
            sys.argv.pop(1) #ignore this argument, it's not a language or a file
        
    
    if len(languages) == 0 or len(files) == 0:
        print("usage: python translator_writer.py -l <language1> [language2] ... -f <file1> [file2] ...")
        sys.exit(1)
    
    pb = ProgressBar(len(languages), "Creating translation files")
    for language in languages:
        pb += 1
        create_translate_file(files, language)
    del pb