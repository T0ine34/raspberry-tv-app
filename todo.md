# What is this project ?
This project is a application that simplify the usage of a raspberry pi on a TV.
It will be able to play music, videos, show pictures, and display a clock.
It also allow to open some specifics websites, like youtube, netflix, etc.
It also allow to completly overpass the OS, and shutdown or reboot the raspberry pi.

# TODO list
- [x] Implements a interface using larges buttons
- [x] Add a clock
- add some applications :
    -  [ ] Music player
    -  [ ] Video player
    -  [ ] Picture viewer
    -  Web explorer :
        - [x] Youtube
        - [x] Netflix
        - [ ] Twitch
        - [ ] Amazon prime video
    - [x] Shutdown / reboot
- Add a settings menu
    - [ ] Add a way to change the background
    - [ ] Add a way to change the theme
    - [ ] Add a way to change the language
    - [ ] Add a way to change the clock format
- [ ] Add a custom mouse cursor (done on windows, but not on linux)
- [ ] Add a way to change the volume
- [ ] Add a way to change the brightness

# How to use it ?
clone this repository, and run the launcher.py file. It will launch the application, and install the dependencies if needed.
