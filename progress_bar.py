import os
from math import ceil, floor

def goup(n : int = 1):
    for i in range(n):
        print("\033[A", end="")

class ProgressBar:
    QUANTITY = 0 #quantity of progress bars active
    def __init__(self, total : int, title : str = ""):
        self.total = total
        self.title = title
        self.current = 0
        
        if self.__class__.QUANTITY > 0:
            print()
        self.__class__.QUANTITY += 1
        
    def __del__(self):
        total_width = os.get_terminal_size().columns
        print(" " * total_width, end="\r")
        self.__class__.QUANTITY -= 1
        if self.__class__.QUANTITY > 0:
            goup()
        
    def update(self, value : int):
        self.current = value
        self.draw()
        
    def draw(self):
        total_width = os.get_terminal_size().columns
        text_width = len(self.title) + len(str(self.current)) + len(str(self.total)) + 8
        if text_width > total_width:
            self.title = self.title[:total_width - text_width - 3] + "..."
            text_width = len(self.title) + len(str(self.current)) + len(str(self.total)) + 8
        print(f"{self.title} : {self.current}/{self.total} [{'=' * floor((self.current / self.total) * (total_width - text_width))}{' ' * ceil((1 - (self.current / self.total)) * (total_width - text_width))}]", end="\r")
        
    def __iadd__(self, value : int):
        self.update(self.current + value)
        return self
    
    def __isub__(self, value : int):
        self.update(self.current - value)
        return self
    
if __name__ == "__main__":
    from time import sleep
    
    pb = ProgressBar(10, "main action")
    for i in range(10):
        pb += 1
        pb2 = ProgressBar(5, "sub action")
        for j in range(5):
            pb2 += 1
            sleep(0.1)
        del pb2
    del pb