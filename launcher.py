from translator import Translator as tr
import sys

EXEC = sys.executable + " main.py __launcher__"
RELOAD_LANGUAGES_EXEC = sys.executable + " translator_writer.py -f %s -l %s"

import os

def start():
    return os.system(EXEC)
    
def reload_languages(files : list, languages : list = tr.get_available_languages()) -> int:
    """
    parameters:
        files: list of files to translate
        languages: list of languages to translate the files into
    
    return:
        0: normal exit
        1: error
    """
    return os.system(RELOAD_LANGUAGES_EXEC % (" ".join(files), " ".join(languages)))
    
if __name__ == "__main__":
    exit_code = start()
    error = False
    while exit_code != 0: # exit(0) - normal exit, no need to reboot
        if exit_code == 256 or exit_code == 1: # exit(1) - error; try to reboot
            if error:
                break
            error = True
            exit_code = start()
        elif exit_code == 512 or exit_code == 2: # exit(2)  - reload languages and reboot
            if reload_languages(["main.py"]) != 0:
                print("Could not reload languages, exiting...")
                break
            exit_code = start()
            error = False
        elif exit_code == 768 or exit_code == 3: # exit(3) - reboot
            exit_code = start()
            error = False
        elif exit_code == 1024 or exit_code == 4: # exit(4) - install modules and reboot
            import installer
            if not installer.install_file("requirements.txt"):
                print("Could not install the required modules, exiting...")
                break
            else:
                print("Modules installed successfully")
            exit_code = start()
            error = False
        else:
            print("Unknown exit code: %d" % exit_code)
            break