
from json import load as json_load
import subprocess

def open_website(website : str) -> None:
    with open("url.json", "r") as f:
        urls = json_load(f)
    if website in urls:
        try:
            subprocess.call(["chromium-browser", urls[website]])
        except FileNotFoundError:
            try:
                subprocess.call(["start", "chrome", urls[website]])
            except FileNotFoundError as e:
                raise FileNotFoundError("Could not find chromium-browser or chrome") from e
    else:
        raise ValueError(f"Unknown website : '{website}'")
    
if __name__ == "__main__":
    open_website("youtube")