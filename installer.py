#install all module in requirements.txt with the python interpreter currently in use

import sys
import subprocess

def install(package):
    #call the command "python -m pip install --upgrade <package>" in the terminal
    subprocess.check_call([sys.executable, "-m", "pip", "install", "--upgrade", package], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def install_file(file : str):
    try:
        with open(file) as f:
            for line in f:
                try:
                    print("installing " + line.strip('\n')+"... ",end="") #remove \n
                    install(line)
                except:
                    print("failed")
                    return False
                else:
                    print("done")
        return True
    except FileNotFoundError:
        print("could not find " + file)
        return False

if __name__ == "__main__":
    install_file("requirements.txt")