
#------------------------------------------------------ IMPORTS ------------------------------------------------------#

import sys

try:
    import customtkinter as ctk #need to be installed
    import tkinter as tk

    from PIL import Image, ImageTk #need to be installed

    from typing import Callable

    from datetime import datetime
    from time import sleep
    from threading import Thread

    from osinfo import *
    from translator import Translator as tr
    
    from settings import Settings
    
    from webexplorer import open_website
    
    import os
    
    import json
    
except ImportError as e:
    import subprocess
    if subprocess.call([sys.executable, "installer.py"]) == 0:
        print("Modules installed successfully, restarting...")
        exit(3) #restart the program if started in the launcher
    else:
        raise ImportError("Could not install the required modules") from e
    
    
#-------------------------------------------------- FUNCTIONS & CLASS ------------------------------------------------#

def _is_type_of(path : str, type : str, config_file : str = "extensions.config") -> bool:
    with open(config_file, "r") as f:
        extensions = json.load(f)[type]
    for extension in extensions:
        if path.endswith(extension):
            return True
    return False

def is_image(path : str) -> bool:
    return _is_type_of(path, "image")
def is_text(path : str) -> bool:
    return _is_type_of(path, "text") or _is_type_of(path, "code")
def is_video(path : str) -> bool:
    return _is_type_of(path, "video")

class Explorer(ctk.CTkFrame):
    def __init__(self, master, path : str, preview=True, show_path=True):
        super().__init__(master)
        
        self.path = tk.StringVar(value=path)
        
        self.preview = preview
        self.show_path = show_path
        
        self.selected = []
        
        self.last_selected = None
        
        self._init_widgets()
        
        self._update()
        
    def _init_widgets(self):
        self._init_preview()
        self._init_path()
        self._init_listbox()
        
    def _init_preview(self):
        #it's a frame that display the current selected file (text or image)
        #only preview a file if it's the only one selected
        #if multiple files are selected or 0, the preview is empty
        
        
        
        self.preview_frame = ctk.CTkScrollableFrame(self)
        self.preview_frame.pack(side=tk.RIGHT, expand=True, fill=tk.BOTH)
        
        self.preview_frame.columnconfigure(0, weight=1) #the preview will take all the space
        self.preview_frame.rowconfigure(0, weight=1) #the preview will take all the space
        
        self.preview_frame.pack_propagate(False) #don't resize the preview
        self.preview_frame.bind("<Configure>", self._update_preview) #update the preview when the frame is resized
        
        self.preview_frame.grid_remove()

    def _init_path(self):
        #it's a entry that display the current path, and allow modifying it
        self.path_entry = ctk.CTkEntry(self, width=1, textvariable=self.path, font=("Arial", 25))
        self.path_entry.pack(side=tk.TOP, fill=tk.X, expand=False)
        
        self.path_entry.bind("<Return>", self._on_path_change)
        self.path_entry.bind("<FocusOut>", self._on_path_change)
        
        
        self.path_entry.grid_remove()
        
    def _init_listbox(self):
        #it's the main element, that display the files and folders in the current path
        self.listbox = tk.Listbox(self, selectmode=tk.EXTENDED, font=("Arial", 25))
        self.listbox.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.listbox.bind("<Double-Button-1>", self._on_double_click)
        self.listbox.bind("<Button-1>", self._on_click)
        self.listbox.bind("<ButtonRelease-1>", self._on_release)
        self.listbox.bind("<B1-Motion>", self._on_drag)
        
        self.listbox.bind("<Button-3>", self._on_right_click)
        self.listbox.bind("<ButtonRelease-3>", self._on_right_release)
        
    def _update(self):
        self._update_path()
        self._update_preview()
        self._update_listbox()
        
    def _update_preview(self, event=None):
        if not self.preview:
            return
            
        if len(self.selected) == 1:
            selected = self.selected[0]
            if os.path.isfile(selected): #if it's a file
                if not self.last_selected == selected:
                    if is_image(selected):
                        self._display_image(selected)
                    elif is_text(selected):
                        self._display_text(selected)
                    elif is_video(selected):
                        pass
                    else:
                        self._clear_preview()
                else: #if it's the same file, do nothing
                    return
            else:
                self._clear_preview()
            
            self.last_selected = selected
        else:
            self._clear_preview()
            
            
    def _update_path(self):
        if not self.show_path:
            return
            
        self.path_entry.pack()
        self.path_entry.configure(state=tk.NORMAL)
        
    def _update_listbox(self):
        self.listbox.delete(0, tk.END)
        
        try:
            for file in os.listdir(self.path.get()):
                self.listbox.insert(tk.END, file)
        except FileNotFoundError:
            self.path.set(os.path.dirname(self.path.get()))
            self._update()
            print("File not found")
        except PermissionError:
            self.path.set(os.path.dirname(self.path.get()))
            self._update()
            print("Permission denied")
        except Exception as e:
            print(e)

    def _display_image(self, path : str):
        self._clear_preview()
        
        self.image = Image.open(path)
        self.image = self.image.resize((self.preview_frame.winfo_width(), self.preview_frame.winfo_height()))
        self.ctkimage = ctk.CTkImage(self.image)
        
        zone_width = self.preview_frame.winfo_width()
        ratio = zone_width // self.image.width #ratio between the width of the image and the width of the zone
        zone_height = self.image.height * ratio #height of the zone
        
        print(zone_width, zone_height)
        
        self.image_label = ctk.CTkLabel(self.preview_frame, image=self.ctkimage, text="", width=zone_width, height=zone_height)
        self.image_label.pack(expand=True, fill=tk.BOTH)
        
        self.preview_frame.pack()
        
    def _display_text(self, path : str):
        self._clear_preview()
        
        with open(path, "r") as f:
            text = f.read(10000) #read the first 1000 characters of the file (if it's too big, it will be truncated)
        
        print(text)
        self.text_label = ctk.CTkLabel(self.preview_frame, text=text)
        self.text_label.grid(row=0, column=0, sticky="nsew")
        
        self.preview_frame.pack()
        
    def _clear_preview(self):
        self.preview_frame.grid_remove()
        
        for child in self.preview_frame.winfo_children():
            child.destroy()
            
    def _on_path_change(self, event=None):
        self._update()
        
    def _on_double_click(self, event=None):
        for item in self.listbox.curselection():
            item = self.listbox.get(item)
            path = os.path.join(self.path.get(), item)
            if os.path.isdir(path):
                self.path.set(path)
                self._update()
            else:
                continue #TODO : open file
                open_file(path)
                
    def _on_click(self, event=None):
        self.selected = []
        for item in self.listbox.curselection():
            self.selected.append(os.path.join(self.path.get(), self.listbox.get(item)))
            
        self._update_preview()
        
    def _on_release(self, event=None):
        self._on_click()
        
    def _on_drag(self, event=None):
        self._on_click()
        
    def _on_right_click(self, event=None):
        self._on_click()
        
    def _on_right_release(self, event=None):
        self._on_click()
        
    def get_selected(self) -> list:
        return self.selected
    
    def get_path(self) -> str:
        return self.path.get()
    
    
if __name__ == "__main__":
    root = ctk.CTk()
    
    explorer = Explorer(root, "C:\\Users\\antoi", False, False)
    explorer.pack(expand=True, fill=tk.BOTH)
    
    root.mainloop()