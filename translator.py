import json
import os.path


import inspect

class TranslationError(Exception):
    pass

class TranslationFileNotFoundError(TranslationError):
    pass

class TranslationNotFoundError(TranslationError):
    def __str__(self):
        return f"{super().__str__()}; try regerating the translation file"

class Translator:
    def __init__(self, language : str, path : str = "translations/"):
        self.language = language
        self.translations = []
        
        if not path.endswith("/"):
            path += "/"
            
        self.path = path+language+".json"
        
        os.makedirs(path, exist_ok=True)

        self.load()
        
    def load(self):
        try:
            with open(self.path, "r") as f:
                self.translations = json.load(f)
            print(f"file '{self.path}' loaded")
        except FileNotFoundError:
            with open(self.path, "w") as f:
                json.dump([], f)
            print(f"file '{self.path}' created")
            self.translations = []
            exit(2) # reload languages and reboot
        except json.decoder.JSONDecodeError as e:
            print(f"Error while decoding '{self.path}': {e}")
            exit(1)
        
    def translate(self, key : str):
        """
        key: the string to translate
        """
        caller_filename = inspect.stack()[2].filename   # the caller is the function that called the function that called this function
        caller_line = inspect.stack()[2].lineno         # here, we want the name of the file and the line of the caller of the caller of this function
                                                        # this give us the line and the file of the string that we want to translate
        
        for translation in self.translations:
            try:
                if (translation["string"] == key and
                    os.path.relpath(translation["file"]) == os.path.relpath(caller_filename) and
                    translation["line"] == caller_line
                ):
                    return translation["traduction"]
            except ValueError as e:
                print(e)
                print(f"caller_filename: {caller_filename}: {caller_line}")
                print(f"translation file: {translation['file']}: {translation['line']}")
                exit(1)
        print(TranslationNotFoundError(f"Translation for key '{key}' (line {caller_line} in '{os.path.relpath(caller_filename)}') for language '{self.language}' not found"))
        exit(2) # reload languages and reboot
    
    def __getitem__(self, key : str): #usage: translator["key"]
        return self.translate(key)
    
    def __call__(self, key : str):    #usage: translator("key")
        return self.translate(key)
    
    def __repr__(self):
        return f"Translator({self.language})"
    
    def __str__(self):
        return f"Translator({self.language})"
    
    
    @staticmethod
    def get_available_languages(path : str = "translations/") -> list:
        if not path.endswith("/"):
            path += "/"
        
        if os.path.isdir(path):
            files = os.listdir(path)
            return [file[:-5] for file in files if file.endswith(".json")]
        else:
            return ["en"]
        